package com.example.testrecyclerview;

import android.content.Intent;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class MemberAdp extends RecyclerView.Adapter<MemberAdp.ViewHolder> {
    ArrayList<String> arrayListMember;
    ArrayList<String> arrayListJob ;
    String name;
    View mView;
    MainActivity data = new MainActivity();
    Filter names = new Filter();

    public MemberAdp(){

    }
    public MemberAdp(ArrayList<String> arrayListMember,ArrayList<String> arrayListJob){
        this.arrayListMember = arrayListMember;
        this.arrayListJob = arrayListJob;

    }
    @NonNull
    @NotNull
    @Override
    public MemberAdp.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
       mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_member,parent,false);
        return new MemberAdp.ViewHolder(mView);
    }
    @Override
    public void onBindViewHolder(@NonNull @NotNull MemberAdp.ViewHolder holder, int position) {
        Log.d("pos", "Pos"+String.valueOf(position));
        Log.d("pos", "arrayListMember "+arrayListMember.size());
        Log.d("pos", "arrayListJob "+(arrayListJob.size()));

        holder.tvName.setText(arrayListMember.get(position));
        holder.tvJob.setText(arrayListJob.get(position));
        holder.imageView.setImageResource(genPictureNum());


        holder.a.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name = (String) holder.tvName.getText();
               // Filter names = new Filter();
                names.setData(name);
                arrayListMember.remove(name);

                 holder.card.setCardBackgroundColor(1);


                //holder.card.setVisibility(View.INVISIBLE);

            }
        });
    }


    @Override
    public int getItemCount() {
        return arrayListMember.size();
    }
    private int genPictureNum() {
        int i = (int)(Math.random()*5);
        switch (i){
             case 1:return R.drawable.p2;
             case 2:return R.drawable.p3;
             case 3:return R.drawable.p4;
            default:return R.drawable.p5;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvJob;
        ImageView imageView;
        Button a;
        CardView card;
        Boolean toggle ;
        SupMain data = new SupMain();

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            tvJob = itemView.findViewById(R.id.tv_job);
            imageView = itemView.findViewById(R.id.imageView);
            a=itemView.findViewById(R.id.remove_one);
             card = itemView.findViewById(R.id.cv_member);
        }
    }
}
