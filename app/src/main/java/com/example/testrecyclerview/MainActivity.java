package com.example.testrecyclerview;

import static android.view.View.VISIBLE;
import static android.view.View.inflate;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Member;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView rvRank,rvMember;
    Button bttRemove,bttYes,bttNo,removeOlny;
    LinearLayoutManager linearLayoutManagerRank;
    RangAdp adapterRank;
    TextView edtSearch;
    ConstraintLayout conRemove;
    SupMain data = new SupMain();


    ArrayList dataNewMem = new ArrayList();
    ArrayList dataNewAd = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);

        removeOlny = findViewById(R.id.btt_remove_olny);
        edtSearch = findViewById(R.id.tv_Search);
        bttRemove = findViewById(R.id.btt_remove);
        bttYes = findViewById(R.id.btt_Yes);
        bttNo = findViewById(R.id.btt_No);
        conRemove =  findViewById(R.id.con_Remove);
        rvRank = findViewById(R.id.rv_Rank);
        rvMember = findViewById(R.id.rv_member);
        conRemove.setVisibility(View.INVISIBLE);




       // data.getDataListMember();
        Log.d("TagAAAA","AAAA "+data.getDataListMember());
        data.getDataListMember().add("a");
        Log.d("TagAAAA","AAAA "+data.getDataListMember());

        adapterRank = new RangAdp(data.addDataRank("Admin","Member"),data.addDataListMember(),data.addDataListMemberAdmin());
        linearLayoutManagerRank = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvRank.setLayoutManager(linearLayoutManagerRank);
        rvRank.setAdapter(adapterRank);

        removeOlny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                adapterRank = new RangAdp(data.arrayListRang,data.arrayListMember,data.arrayListAdmin);
                linearLayoutManagerRank = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                rvRank.setLayoutManager(linearLayoutManagerRank);
                rvRank.setAdapter(adapterRank);
                edtSearch.setText("");
            }
        });


        bttYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(Object i : dataNewMem){
                        data.arrayListMember.remove(i);
                        data.arrayListAdmin.remove(i);
               }
                for(Object i : dataNewAd){
                    data.arrayListMember.remove(i);
                    data.arrayListAdmin.remove(i);
                }
                adapterRank = new RangAdp(data.arrayListRang,data.arrayListMember,data.arrayListAdmin);
                linearLayoutManagerRank = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                rvRank.setLayoutManager(linearLayoutManagerRank);
                rvRank.setAdapter(adapterRank);
                edtSearch.setText("");
                conRemove.setVisibility(View.INVISIBLE);
            }
        });
        bttNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterRank = new RangAdp(data.arrayListRang,data.arrayListMember,data.arrayListAdmin);
                linearLayoutManagerRank = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                rvRank.setLayoutManager(linearLayoutManagerRank);
                rvRank.setAdapter(adapterRank);
                conRemove.setVisibility(View.INVISIBLE);
            }
        });
        bttRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conRemove.setVisibility(VISIBLE);

            }
        });
        bttRemove.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d("remove","onLongClick ");
                return true;
            }
        });
        bttRemove.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                Log.d("TagTextChanged","_______________");
                Log.d("TagTextChanged"," "+s.toString());
                Log.d("TagTextChanged","start "+start);
                Log.d("TagTextChanged","count "+count);
                Log.d("TagTextChanged","_______________");


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("TagTextChanged","_______________");
                Log.d("TagTextChanged"," "+s.toString());
                Log.d("TagTextChanged","start "+start);
                Log.d("TagTextChanged","before "+before);
                Log.d("TagTextChanged","count "+count);
                Log.d("TagTextChanged","_______________");
            }

            @Override
            public void afterTextChanged(Editable s) {


                    String str = s.toString();

                dataNewMem.clear();
                dataNewAd.clear();
                    edtSearch.removeTextChangedListener(this);
                    adapterRank = new RangAdp(data.arrayListRang, data.arrayListMember, data.arrayListAdmin);
                    linearLayoutManagerRank = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                    rvRank.setLayoutManager(linearLayoutManagerRank);
                    rvRank.setAdapter(adapterRank);


                if( !str.equals("")) {
                    for (String i : data.arrayListMember) {

                            if (i.contains(str)) {
                                 dataNewMem.add(i);
                            }
                    }
                    for (String i : data.arrayListAdmin) {

                        if (i.contains(str)) {
                            dataNewAd.add(i);
                        }
                    }
                    Filter a = new Filter( dataNewMem,dataNewAd);
                    adapterRank = new RangAdp(data.arrayListRang, dataNewMem,dataNewAd);
                   // adapterRank = new RangAdp(data.arrayListRang, data.arrayListAdmin, data.arrayListAdmin );
                    linearLayoutManagerRank = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                    rvRank.setLayoutManager(linearLayoutManagerRank);
                    rvRank.setAdapter(adapterRank);

                }
                edtSearch.addTextChangedListener( this );

            }
        });
    }

}