package com.example.testrecyclerview;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class RangAdp extends RecyclerView.Adapter<RangAdp.ViewHolder> {

    SupMain sent = new SupMain();
    ArrayList<String> arrayListGroup,addDataListMemberAdmin,addDataListMember,dataJop;
    private View mView;


        public RangAdp(ArrayList<String> arrayListGroup,ArrayList<String> addDataListMember,ArrayList<String> addDataListMemberAdmin){
            this.arrayListGroup = arrayListGroup;
            this.addDataListMember = addDataListMember;
            this.addDataListMemberAdmin = addDataListMemberAdmin;
            this.dataJop=sent.addDataListJob();
        }





    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {
         mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_group,parent,false);
         return new RangAdp.ViewHolder(mView);
    }
    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull RangAdp.ViewHolder holder, int position) {
            holder.tvName.setText(arrayListGroup.get(position));

        Log.d("pos", "__"+String.valueOf(position));
            //Log.d("Tagtest", "activity_null "+String.valueOf(mView.getContext()==null));
            LinearLayoutManager layoutManagerMemberVe = new LinearLayoutManager(mView.getContext(), LinearLayoutManager.VERTICAL, false);
            LinearLayoutManager layoutManagerMemberHo = new LinearLayoutManager(mView.getContext(), LinearLayoutManager.HORIZONTAL, false);
            ArrayList<String> arrayListMember = new ArrayList<>();
// arrayListJob.contains("ss");
        if(position==0) {
            Log.d("TagMain","Admin "+arrayListMember);
            MemberAdp adapterMember = new  MemberAdp(addDataListMemberAdmin,this.dataJop);
            holder.rvMember.setLayoutManager(layoutManagerMemberVe);
            holder.rvMember.setAdapter(adapterMember);
       }
      else if(position==1) {
            MemberAdp adapterMember = new MemberAdp(addDataListMember,this.dataJop);
            Log.d("TagSentDataSup","Sup  "+addDataListMember);
            holder.rvMember.setLayoutManager(layoutManagerMemberVe);
            holder.rvMember.setAdapter(adapterMember);

            Log.d("TagMain","Member "+arrayListMember);
       }else {
            Log.d("TagMain","Nodata "+arrayListMember);
       }
    }
    @Override
    public int getItemCount() {
        return arrayListGroup.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
    TextView tvName;
    RecyclerView rvMember;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            rvMember = itemView.findViewById(R.id.rv_member);
        }
    }


}
